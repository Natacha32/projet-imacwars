#include "tools.h"

int runGame(bool choix);




void draw(Jeu* jeu, GLuint textures[], int x_clic, int y_clic, int x_mot, int y_mot, int indexSelectedUnit, Joueur *activePlayer);

// SI LA CASE CLIQUE A UNE UNITE (x,y) DESSUS, MET EN SURBRILLANCE LES CASES OU CELLE-CI PEUT SE DEPLACER
void highlightPossibleCases(Jeu* jeu, GLuint texture, int indexSelectedUnit, Joueur* activePlayer);


void unitAction(Jeu* jeu, int id_actif, int x_clic, int y_clic, int *indexSelectedUnit);
int isYourUnusedUnit(int x_clic, int y_clic, int id_actif, Jeu* jeu);
bool isPossibleCase(int x_new, int y_new, Unite* unite, Jeu* jeu);
bool isEnemyWithinRange(int x, int y, int id_enemy, Unite* yourUnit, Unite*& enemyUnit, Jeu* jeu);
void deplacement(Unite* unite_active, int x_clic, int y_clic);
void attaque(Unite* yourUnit, Unite* enemyUnit, Jeu* jeu, int id_actif);
int** casesWithinAtkRange(Unite unite, int* nb, Jeu *jeu, bool moved);
bool attackAfterMovePossible(Unite yourUnit, Jeu* jeu, int id_actif);
void greyUsedUnits(Joueur activePlayer, GLuint texture);
void highlightAtkCases(Jeu* jeu, GLuint texture, int indexSelectedUnit, Joueur* activePlayer) ;





Armee* initallUnits(Jeu* jeu);
void supprimerUnite(Armee *armee, Unite* unite);

// RENVOIE LE TABLEAU DES COORD DE CASES OU L'UNITE PEUT SE DEPLACER
// INCOMPLET
//int** possibleCases(Unite unite, int* nb, Jeu* jeu);
void possibleCases(Unite unite, Jeu* jeu, int* nb, int**& cases, int h, int b, int d, int g);
bool movePossible(int x_clic, int y_clic, Unite* unite, Jeu* jeu);
int** casesWithinAtkRange(Unite unite, int* nb, Jeu *jeu, Joueur activePlayer);
void drawEndPanel(int winner, Jeu* jeu);
void infoPanel();