#include <iostream>
using namespace std;
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "fonctions.h"


bool isDestination(Coordonnees* current, Coordonnees* dest){
  if((current->x == dest->x) && (current->y = dest->y)){
    return true;
  }
  return false;
}

Coordonnees* createCase(int x, int y){
  Coordonnees* cellule = (Coordonnees*)malloc(sizeof(Coordonnees));
  cellule->x = x;
  cellule->y = y;
  return cellule;
}

//Calcul de distance
void calculCost(Coordonnees* first, Coordonnees *carte, Coordonnees* start, Coordonnees* dest, int taille_carte){

    calculHCostDiagonal(start, carte, taille_carte);
    calculGCostDiagonal(dest, carte, taille_carte);
    calculFCost(carte);
}

void calculHCostDiagonal(Coordonnees* start, Coordonnees *carte, int taille_carte){
    //int h_cost = isDiagonal(carte, start, taille_carte);
    //cout<< "le resultat de isDiagonal est " << h_cost << endl;
    /*if(h_cost != 0){
      carte->h_cost += h_cost;
    }
    else{*/
      carte->h_cost = distanceCase(start, carte);
    //}
}

void calculGCostDiagonal(Coordonnees* dest, Coordonnees *carte, int taille_carte){
    //int g_cost = isDiagonal(carte, dest, taille_carte);
    //cout<< "le resultat de isDiagonal est " << g_cost << endl;
    /*if(g_cost != 0){
      carte->g_cost += g_cost;
    }
    else{*/
      carte->g_cost = distanceCase(dest, carte);
    //}
}

int distanceCase(Coordonnees* start, Coordonnees* cellule){
    int resultx = (abs(cellule->x - start->x)) *10;
    int resulty;
    if(start->y == cellule->y){
         resulty = 0;
        }
    else{
       resulty = (abs(cellule->y - start->y)) *10;
    }
    int total = resultx + resulty;
  return total;
}

void calculFCost(Coordonnees* autre){

  int g_cost = autre->g_cost;
  int h_cost = autre->h_cost;
  autre->f_cost = g_cost + h_cost;
}

//Si on prend les cases en diagonal
/*int isDiagonal(Coordonnees* cellule, Coordonnees* start, int taille_carte){
  int x = start->x;
  int y= start->y;
  for(int i; i< taille_carte; i++){
    if( ( (cellule->x == (x-=i)) && (cellule->y == (y-=i)) ) || ( (cellule->x == (x+=i)) && ((cellule->y) == (y-=i))) || ((cellule->x == (x+=i)) && (cellule->y == (y+=i))) || ((cellule->x == (x-=i)) && (cellule->y == (y+=i)))){
      if(i == 1)
        return 14;
      else
        return i*14;
    }
  }
  return 0;
}*/

bool isObstacle(Coordonnees* cellule, Armee* armeeInverse){
  for(int i = 0; i< armeeInverse->taille; i++){
    if(cellule->x == armeeInverse->unites[i].coordonnees[1] && cellule->y == armeeInverse->unites[i].coordonnees[0]){
      return true;
    }
  }
  return false;
}

void tri(Coordonnees* tab[4]){
    cout << "entré222222" << endl;
  Coordonnees *tabtrans[2] = {NULL, NULL};
  for(int i=0; i<4; i++) //A MODIFIER POUR PRENDRE EN COMPTE LES BORDS
  {
      for(int j=3; j>=i; j--)
      {
          if(tab[i] !=NULL && tab[j]!= NULL){
            if(tab[i]->f_cost > tab[j]->f_cost){
                tabtrans[0]= tab[i];
                tabtrans[1]= tab[j];
                tab[i]= tabtrans[1];
                tab[j] = tabtrans[0];
            }
          }
      }

  }
}


Coordonnees* lookSiblings(Coordonnees* first, Coordonnees* start, Coordonnees* dest, Coordonnees* to_visit[], Coordonnees* visited[], int taille_carte){
    //Réception des données de la case courante
    //start correspond à la case courante
    int x_start = start->x;
    int y_start = start->y;
    cout << "x_start " << x_start << "\n";
    cout << "y_start " << y_start << "\n";
    cout << "x_start-1 " << x_start-1 << "\n";
    cout << "y_start-1 " << y_start << "\n";
    cout << createCase((x_start -1),y_start)->x;
    //Création d'un tableau qui rescence tous les voisins de la case
    Coordonnees* siblings[4] = {NULL};
    int count = 0;
    if((x_start-1)>0){
        siblings[count] = createCase((x_start -1),y_start);
        cout << "siblings["<<count<< "]"<< siblings[count]->x<<"\n" << endl;
        cout << "siblings["<<count<< "]"<< siblings[count]->y<<"\n" << endl;
        count += 1;

    }
    if((y_start + 1)<taille_carte){
        siblings[count] = createCase(x_start, (y_start + 1));
         cout << "siblings["<<count<< "]"<< siblings[count]->x<<"\n" << endl;
        cout << "siblings["<<count<< "]"<< siblings[count]->y<<"\n" << endl;
        count +=1;

    }

    if((y_start - 1) > 0){
        siblings[count] = createCase(x_start, (y_start - 1));
        cout << "siblings["<<count<< "]"<< siblings[count]->x<<"\n" << endl;
        cout << "siblings["<<count<< "]"<< siblings[count]->y<<"\n" << endl;
        count +=1;

    }
    if((x_start +1)<taille_carte)
    {
       siblings[count] = createCase((x_start +1), y_start);
       cout << "siblings["<<count<< "]"<< siblings[count]->x<<"\n" << endl;
        cout << "siblings["<<count<< "]"<< siblings[count]->y<<"\n" << endl;
    }


    for(int i=0; i<4; i++){
        if(siblings[i] !=NULL){
            calculCost(first, siblings[i], start, dest, taille_carte);
            if(start->parent != siblings[i])
                siblings[i]->parent = start;
        }
    }
    tri(siblings);

    int count_visit = 0;
    for(int i=1; i<4; i++){
        if(siblings[i] !=NULL){
            while(to_visit[count_visit] != NULL)
                count_visit +=1;
            to_visit[count_visit] = siblings[i];
        }
    }

    return siblings[0];
}


int AStar(Coordonnees* start, Coordonnees* dest, Armee* armeeInverse, Armee* artificial, int taille_carte, int dexterite, Coordonnees* tabDep[]){
    //Création des tableaux des listes
    Coordonnees* to_visit[100] = {NULL};
    Coordonnees* visited[100] = {NULL};
    //Valeur intermédiaire fixée à la case de départ
    Coordonnees* current = start;
    int count = 0;
    int count_to_visit = 0;
    //Création d'une boucle qui vérifie si la case courante est la case de destination
    while(!isDestination(current, dest)){
        //Insertion de la case la plus proche dans visited
        visited[count] = lookSiblings(start, current, dest, to_visit, visited, taille_carte);
        //Verification de la présence d'un obstacle
        if(isObstacle(current, armeeInverse)){
            //Si la coordonnée est un obstacle, on l'ignore et on revient à la coordonnée la plus proche après elle
            visited[count] = NULL;
            count_to_visit +=1;
            current = to_visit[count_to_visit];//PAS SUR
        }
        else{
            //La case courante devient la case reçue => on se déplace vers la case la plus proche
            current = visited[count];
            count+= 1;
        }
    }
    int nbrCases = 0;
    //On remonte la liste grâce au parents à partir de la coordonnée de destination jusqu'à la coordonnée de départ en comptant le nombre de cases traversées
    while(current!=start){
        current = current->parent;
        nbrCases += 1;
    }

    if(visited[dexterite] !=NULL){
      tabDeplacement(visited[dexterite]->x, visited[dexterite]->y, tabDep);
    }
  return nbrCases;
}

Coordonnees* tabDeplacement(int x, int y, Coordonnees* tab[]){
    Coordonnees* cellule =(Coordonnees*)malloc(sizeof(Coordonnees));
    cellule->x = x;
    cellule->y = y;
    for(int i=0; i<sizeof(tab); i++){
        if(tab[i]==NULL){
          tab[i]=cellule;
          break;
        }

    }
}
