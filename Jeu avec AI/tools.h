#include "fonctions.h"

// GESTION DES UNITES ET INITIALISATION

Unite *creerFantassin(int joueur);
Unite *creerCavalier(int joueur);
Unite *creerArcher(int joueur);
void coordIni(Jeu* jeu);
void ajouterUnites(Armee *armee);
void lireArmee(Armee *armee);
void lireUnite(Unite unite);



Jeu* initGame(bool choix);
Joueur* initPlayer(int id, int fond);
void reinitStats(Jeu* jeu);
void reinitUtilisee(Joueur* joueur);
void reinitCurrent(Joueur* joueur);
int endGame(int taille1, int taille2);
bool usedAllUnits(Armee* armee);



// CARTE

void displayArray2D(int** tab2D, int n, int m);
// GENERE UN NB ALEATOIRE ENTRE 0 ET n-1 AVEC CERTAINES PROB STOCKEES DANS p[]
int randomNumber(int n, float p[]);
// GENERE LE TABLEAU 2D DE LA CARTE DE DIM n*n
// 1->plaine, 2->foret2, 3->montagne, 4->forteresse 
int** createMap();
// APPLIQUE LES EFFETS DES CASES AUX UNITES QUI SE TROUVENT DESSUS 
void CaseEffectsOn(Jeu* jeu);



// ECRIT UN TEXTE
void bitmapOutput(float x, float y, float r, float g, float b, const char *string, void *font);


// CHARGE L'IMAGE
SDL_Surface* image(const char* img_path);
// INITIALISE LA TEXTURE
int initTexture(SDL_Surface* image, GLuint* texture);
void reshape(SDL_Surface** surface, unsigned int width, unsigned int height);
void drawCase();
void drawCasesOfType(int type, GLuint texture, int** carte);
void drawUnitType(Armee *armee, GLuint texture, int type);
// MET EN SURBRILLANCE LES CASES SURVOLE
void highlight(int x, int y, GLuint texture);
void menuInGame(int x_clic, int y_clic);
void showSelectedUnitInfo(int x, int y, Armee* allUnits);
void showActivePlayer(Joueur joueur);
// DESSINE UN TEXTE QUI CHANGE DE COULEUR QUAND ON CLIQUE DESSUS
void drawClickText(float x, float y, int hauteur, int x_clic, int y_clic, const char* string);










