#include <iostream>
using namespace std;
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "fonctions.h"
#include "game.h"


//NB ACTIONS EGALE A NB UNITES ORDI

void artificialIntelligence(Joueur* joueur, Armee* armee1, Armee* armee2, Jeu* jeu){
    int grille_carte = MAP_SIZE; //10
    int taille = (armee1->taille);
    int choix = rand()%armee1->taille;
    cout <<"choix" << choix;
  //VERIFIER SI FONCTIONNE
  if(armee1->unites[choix].pv > 0){
    int y_opp = armee1->unites[choix].coordonnees[0];
    int x_opp = armee1->unites[choix].coordonnees[1];
    cout << "armee 1"<< armee1->unites[choix].coordonnees[1];
    cout << "armee 1"<< armee1->unites[choix].coordonnees[0];
    int x_utilise;
    int y_utilise;
    int indexUnit = 0;
    Coordonnees* oppos = createCase(x_opp, y_opp);
    int* tab = (int*)malloc(sizeof(int));
    Coordonnees* tabDep[armee2->taille];
    //CHERCHER L'UNITE LA PLUS PROCHE POUR ATTAQUER L'ENNEMI
    for(int i=0; i< armee2->taille; i++){
      x_utilise = armee2->unites[i].coordonnees[1];
      y_utilise = armee2->unites[i].coordonnees[0];
      Coordonnees* unite = createCase(x_utilise, y_utilise);
      cout <<"x   "<<unite->x << " "<< unite->y << " y"<<endl;
      cout << "armee 2"<< armee2->unites[i].coordonnees[1];
      cout << "armee 2"<< armee2->unites[i].coordonnees[0];
      int dexterite = armee2->unites[i].dexterite;
      //CALCUL DU CHEMIN LE PLUS COURT POUR ATTAQUER
      int nbrCases = AStar(unite, oppos, armee1, armee2, grille_carte, dexterite, tabDep);
      tab[i] = nbrCases;
      free(&unite);
    }
    for(int cellule = 0; cellule < armee2->taille; cellule++){
      if(tab[cellule]<= tab[indexUnit]){
        indexUnit = cellule;
      }

    }
    int dexterite_unit =armee2->unites[indexUnit].dexterite;
    if(tab[indexUnit]<= dexterite_unit)
        
        attaque(&(armee2->unites[indexUnit]), &(armee1->unites[choix]), jeu, 2);
    else{
      Coordonnees* unite_used = createCase(tabDep[indexUnit]->x, tabDep[indexUnit]->y);
        if(isObstacle(unite_used, armee1)){
          armee2->unites[indexUnit].coordonnees[1] = tabDep[indexUnit]->x+1;
          armee2->unites[indexUnit].coordonnees[0] = tabDep[indexUnit]->y+1;
        }
        else{
          armee2->unites[indexUnit].coordonnees[1] = tabDep[indexUnit]->x;
          armee2->unites[indexUnit].coordonnees[0] = tabDep[indexUnit]->y;
        }
        if( ((abs(oppos->x - armee2->unites[indexUnit].coordonnees[1])) <=1) && ((abs(oppos->y -  armee2->unites[indexUnit].coordonnees[0]) <=1))) {
            attaque(&(armee2->unites[indexUnit]), &(armee1->unites[choix]), jeu, 2);
        }

}

    free(&tab);
    free(&oppos);
  }
  else{
    artificialIntelligence(joueur, armee1, armee2, jeu);
  }

}
