#include "game.h"

// FONCTION PRINCIPALE
int runGame(bool choix) {

    Jeu* jeu = initGame(choix);

    // INITIALISATION SDL
    if(SDL_Init(SDL_INIT_VIDEO) == -1) {
        fprintf(stderr, "Impossible d'initialiser la SDL. Fin du programme.\n");
        exit(EXIT_FAILURE);
    }
    SDL_Surface* surface;
    reshape(&surface, WINDOW_WIDTH, WINDOW_HEIGHT);


    // TITRE
	SDL_WM_SetCaption(WINDOW_TITLE, NULL);

    // CHARGE LE TERRAIN ET INITILISE LA TEXTURE
    SDL_Surface* img_plaine = image("img/plaine.png");
    SDL_Surface* img_foret = image("img/foret.png");
    SDL_Surface* img_montagne = image("img/montagne.png");
    SDL_Surface* img_forteresse = image("img/forteresse.png");
    SDL_Surface* img_archer_b = image("img/archer_bleu.png");
    SDL_Surface* img_archer_r = image("img/archer_rouge.png");
    SDL_Surface* img_fantassin_b = image("img/fantassin_bleu.png");
    SDL_Surface* img_fantassin_r = image("img/fantassin_rouge.png");
    SDL_Surface* img_cavalier_b = image("img/cavalier_bleu.png");
    SDL_Surface* img_cavalier_r = image("img/cavalier_rouge.png");
    SDL_Surface* img_surbrillance = image("img/surbrillance.png");
    SDL_Surface* img_atk_highlight = image("img/atk_highlight.png");
    SDL_Surface* img_used = image("img/usedUnit.png");

    // INITIALISE LES TEXTURES
    GLuint textures[TYPE_LAND+2*TYPE_UNITES+1];
    glGenTextures(TYPE_LAND+2*TYPE_UNITES+1, textures);

    initTexture(img_plaine, &textures[0]);
    initTexture(img_foret, &textures[1]);
    initTexture(img_montagne, &textures[2]);
    initTexture(img_forteresse, &textures[3]);
    initTexture(img_archer_b, &textures[4]);
    initTexture(img_archer_r, &textures[5]);
    initTexture(img_fantassin_b, &textures[6]);
    initTexture(img_fantassin_r, &textures[7]);
    initTexture(img_cavalier_b, &textures[8]);
    initTexture(img_cavalier_r, &textures[9]);
    initTexture(img_surbrillance, &textures[10]);
    initTexture(img_atk_highlight, &textures[11]);
    initTexture(img_used, &textures[12]);




    int loop = 1;

    // PARAMETRES INITIAUX
    int x_clic = -TILE_SIZE*(WINDOW_WIDTH/GL_VIEW_SIZE)-10, y_clic = -TILE_SIZE*(WINDOW_WIDTH/GL_VIEW_SIZE)-10;
    int x_mot = -TILE_SIZE*(WINDOW_WIDTH/GL_VIEW_SIZE)-10, y_mot = -TILE_SIZE*(WINDOW_WIDTH/GL_VIEW_SIZE)-10;
    int indexSelectedUnit = -1; //AUCUNE UNITE SELECTIONEE
    int id_actif = 1; // ATTENTION id_actif correspond à joueur[id_actif-1]
    int display = 2; // A CHANGER

    while(loop) {
        // RECUP DU TEMPS AU DBT DE LA BOUCLE
        Uint32 startTime = SDL_GetTicks();

        
        
        // AFFICHAGE

        glClear(GL_COLOR_BUFFER_BIT);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


        if(display == 2) {
            draw(jeu, textures, x_clic, y_clic, x_mot, y_mot, indexSelectedUnit, jeu->joueurs[id_actif-1]);
        }
        else if(display == 3) {
            glClear(GL_COLOR_BUFFER_BIT);
            
            glPushMatrix();
                drawEndPanel(endGame(jeu->joueurs[0]->armee->taille, jeu->joueurs[1]->armee->taille), jeu);
            glPopMatrix();
        }
        else if(display == 4) {
            infoPanel();
        }

        // MISE A JOUR DE LA FENETRE
        SDL_GL_SwapBuffers();

        // EVENMENTS
        SDL_Event e;
        while(SDL_PollEvent(&e)) {

            switch(e.type) {
                case SDL_QUIT:
                    loop = 0;
                    break;

                case SDL_KEYDOWN:
                    if(e.key.keysym.sym == SDLK_p) {
                        reinitUtilisee(jeu->joueurs[id_actif-1]);
                        jeu->allUnits = initallUnits(jeu);
                        id_actif = id_actif%2+1; 
                    }
                    if(e.key.keysym.sym == SDLK_r) {
                        jeu = initGame(true);
                        id_actif = 1;
                        indexSelectedUnit = -1;
                        display = 2;
                    }
                    if(e.key.keysym.sym == SDLK_q) {
                        return EXIT_SUCCESS;
                    }
                    if(e.key.keysym.sym == SDLK_i) {
                        if(display == 4) {
                            display = 2;
                        }
                        else {
                            display = 4;
                        }
                    }
                    break;

                case SDL_MOUSEMOTION:
                    x_mot = e.motion.x;
                    y_mot = e.motion.y;
                    break;

                case SDL_MOUSEBUTTONUP: 

                    cout<<"\n\nCLIC\n";

                    x_clic = e.button.x;
                    y_clic = e.button.y;


                    // VERIFIER SI ARTIFICIEL OU NON

                    
                    if(display == 2) {

                        reinitStats(jeu);
                        //CaseEffectsOn(jeu);

                        // EXECUTION D'UNE ACTION POUR UNE UNITE
                        if(indexSelectedUnit >= 0) {
                            cout<<"LE DISPLAY EST BIEN 2 ET UNE UNITÉ EST SÉLECTIONNÉE"<<endl;
                            unitAction(jeu, id_actif, x_clic/(TILE_SIZE*4), y_clic/(TILE_SIZE*4), &indexSelectedUnit);
                            CaseEffectsOn(jeu);
                            cout<<"EFFET DES CASES ACTIVÉ"<<endl;

                            if(endGame(jeu->joueurs[0]->armee->taille, jeu->joueurs[1]->armee->taille) == 0) {
                                // REINIT INITALLUNITS 
                                cout<<"REINIT INIT ALLUNITS SI LE JEU N'EST PAS TERMINÉ"<<endl;
                                jeu->allUnits = initallUnits(jeu);
                            }
                            
                        }
                        
                        artificialIntelligence(jeu->joueurs[1], jeu->joueurs[0]->armee, jeu->joueurs[1]->armee, jeu);
                        // FIN DU TOUR DU JOUEUR
                        if(usedAllUnits(jeu->joueurs[id_actif-1]->armee)) {
                            cout<<"TOUTES LES UNITES DU JOUEUR "<<id_actif<<" ONT ÉTÉ UTILISÉES. RÉINIT DES UTILISATIONS POUR CES UNITÉS";
                            reinitUtilisee(jeu->joueurs[id_actif-1]);
                            jeu->allUnits = initallUnits(jeu);
                            cout<<"REINIT INIT ALL UNITS A LA FIN DU TOUR"<<endl;
                            id_actif = id_actif%2+1; 
                            cout<<"CHANGEMENT DE TOUR : TOUR DU JOUEUR "<<id_actif<<endl;
                        }
                        // FIN DE LA PARTIE 
                        if(endGame(jeu->joueurs[0]->armee->taille, jeu->joueurs[1]->armee->taille) != 0) {
                            cout<<"\n\n\nEND END END END END END"<<endl;
                            display = 3;
                        }

                        // VERIFIE SI UNE UNITÉ A ÉTÉ SELECTIONNÉ AVANT QU'ON CLIQUE ET SI OUI LA LIE À selectedUnit
                        // LES CHANGEMENTS EFFECTUÉS SUR selectedUnit SONT AUSSI APPLIQUÉS SUR L'UNITÉ SURLAQUELLE ELLE POINTE
                        
                        cout<<"APRÈS AVOIR EVENTUELLEMENT EFFECTUÉ DES ACTIONS, SELECTION D'UNE (NOUVELLE) UNITE SUR LA CASE QU'ON VIENT DE CLIQUER"<<endl;
                        cout<<"'UNITÉ PEUT S'ÊTRE DÉPLACÉE SUR CETTE CASE"<<endl;
                        indexSelectedUnit = isYourUnusedUnit(x_clic/(TILE_SIZE*4), y_clic/(TILE_SIZE*4), id_actif, jeu);

                        cout<<"AFFICHAGE DES UNITES SI LE JEU N'EST PAS TERMINÉ: ALL UNITS\n";
                        lireArmee(jeu->allUnits); 
                        cout<<"JOUEUR 1\n";
                        lireArmee(jeu->joueurs[0]->armee);
                        cout<<"JOUEUR 2\n";
                        lireArmee(jeu->joueurs[1]->armee);
                        cout<<"SUCCES DE LA LECTURE\n";
                    }

                    
                    break;
                default:
                    break;
            }
        }


        // CALCUL DU TEMPS ÉCOULÉ
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        // SI TROP PEU DE TEMPS S'EST ÉCOULÉ, MET EN PAUSE LE PROG
        if(elapsedTime < FRAMERATE_MILLISECONDS) {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    // LIBERATION DES RESSOURCES
    glDeleteTextures(TYPE_LAND+2*TYPE_UNITES+1, textures);
    SDL_FreeSurface(img_plaine);
    SDL_FreeSurface(img_foret);
    SDL_FreeSurface(img_montagne);
    SDL_FreeSurface(img_forteresse);
    SDL_FreeSurface(img_archer_r);
    SDL_FreeSurface(img_archer_b);
    SDL_FreeSurface(img_fantassin_b);
    SDL_FreeSurface(img_fantassin_r);
    SDL_FreeSurface(img_cavalier_b);
    SDL_FreeSurface(img_cavalier_r);
    SDL_FreeSurface(img_surbrillance);
    SDL_FreeSurface(img_atk_highlight);
    //SDL_FreeSurface(img_used);

    SDL_Quit();
    return EXIT_SUCCESS;
}

void draw(Jeu* jeu, GLuint textures[], int x_clic, int y_clic, int x_mot, int y_mot, int indexSelectedUnit, Joueur *activePlayer) {


    

    glEnable(GL_TEXTURE_2D);

    drawCasesOfType(0, textures[0], jeu->carte); // affiche les cases plaine
    drawCasesOfType(1, textures[1], jeu->carte); // affiche les cases foret
    drawCasesOfType(2, textures[2], jeu->carte); // affiche les cases montagne
    drawCasesOfType(3, textures[3], jeu->carte); // affiche les cases forteresse
    drawUnitType(jeu->joueurs[0]->armee, textures[4], 3);
    drawUnitType(jeu->joueurs[0]->armee, textures[6], 1);
    drawUnitType(jeu->joueurs[0]->armee, textures[8], 2);
    drawUnitType(jeu->joueurs[1]->armee, textures[5], 3);
    drawUnitType(jeu->joueurs[1]->armee, textures[7], 1);
    drawUnitType(jeu->joueurs[1]->armee, textures[9], 2);

    highlight(x_clic/(TILE_SIZE*4), y_clic/(TILE_SIZE*4), textures[10]);
    highlightPossibleCases(jeu, textures[10], indexSelectedUnit, activePlayer);
    highlightAtkCases(jeu, textures[11], indexSelectedUnit, activePlayer);
    greyUsedUnits(*activePlayer, textures[12]);
        

    glDisable(GL_TEXTURE_2D);
        
    glPushMatrix();
        // PLACE LE COIN INF GAUCHE DE LA PREMIERE LIGNE AVEC LE COIN SUP GAUCHE DE LA FENETRE
        glTranslatef(-GL_VIEW_SIZE/2, GL_VIEW_SIZE/2./aspectRatio, 0.);

        menuInGame(x_clic*GL_VIEW_SIZE/WINDOW_WIDTH, y_clic*GL_VIEW_SIZE/WINDOW_WIDTH);
        showActivePlayer(*activePlayer);
        showSelectedUnitInfo(x_mot/(TILE_SIZE*4), y_mot/(TILE_SIZE*4), jeu->allUnits);
    glPopMatrix();

    

}


// SI LA CASE CLIQUÉE DE COORD (x,y) DANS LA CARTE A L'UNITE SELECTIONNÉE DESSUS, MET EN SURBRILLANCE LES CASES OU CELLE-CI PEUT SE DEPLACER
void highlightPossibleCases(Jeu* jeu, GLuint texture, int indexSelectedUnit, Joueur* activePlayer) {

    if(indexSelectedUnit<0) {
        //cout<<"L'INDEX EST NÉGATIF : AUCUNE UNITÉ SÉLECTIONNÉE"<<endl;
        return;
    }

    Unite* unite = &(activePlayer->armee->unites[indexSelectedUnit]);

    //cout<<"DONNE LES CASES OU DEPl POSSIBLES POUR UNITE :"<<endl;
    lireUnite(*unite);
    
    if(!unite || !unite->current || unite->utilisee[0]) {
        //cout<<"L'UNITE N'EST PAS DÉFINI OU N'EST PAS SÉLECTIONNÉ OU A DÉJÀ ÉTÉ DÉPLACÉ"<<endl;
        return;
    }
    
    int nb = 0;
    int** cases = NULL;
    possibleCases(*unite, jeu, &nb, cases, 0, 0, 0, 0);
    // int** cases = possibleCases(*unite, &nb, jeu);

    if(cases==NULL) {
        cout<<"Pointeur nul";
        return;
    }

    //cout<<"NB DE CASES POSSIBLES : "<<nb;

    //cout<<"LES CASES OU DEPL POSS SONT DANS LE TAB"<<endl;

    for(int j=0 ; j<nb ; j++) {
        highlight(cases[j][1], cases[j][0], texture);
    }

    //cout<<"CASES EN SURBRILLANCE\n";
    // SI ELLE N'A PAS ÉTÉ DÉPLACÉE
}


void possibleCases(Unite unite, Jeu* jeu, int* nb, int**& cases, int h, int b, int d, int g) {
    // x ET y DOIVENT ÊTRE INITIALISÉS À LA PLUS "GRANDE" VALEUR
    bool first = false;
    if(h==0 && b==0 && g==0 && d==0) {
        first = true;
    }
    // CASE QU'ON VÉRIFIE
    int x_new = unite.coordonnees[1] + d - g;
    int y_new = unite.coordonnees[0] + b - h;

    //printf("x_dep = %d, y_dep = %d ", x_new, y_new);

    if(isPossibleCase(x_new, y_new, &unite, jeu) || first) {
        //cout<<"NB : "<<*nb<<endl;
        bool alreadyIn = false;

        for(int i=0 ; i<*nb ; i++) {
            if(cases[i][1] == x_new && cases[i][0] == y_new) {
                alreadyIn = true;
            }
        }

        if(!alreadyIn && !first) {
            cases = (int**) realloc(cases, sizeof(int*)*(*nb+1));
            //cout<<"possible1\n";
            cases[*nb] = (int*) malloc(sizeof(int)*3);
            //cout<<"possible2\n";
            cases[*nb][1] = x_new;
            //cout<<"possible3\n";
            cases[*nb][0] = y_new;
            //cout<<"possible4\n";
            cases[*nb][2] = abs(d-g)+abs(b-h);
            //printf("cases_x = %d, cases_y = %d\n", cases[*nb][1], cases[*nb][0]);
            (*nb)++;
            //cout<<"possible5\n";
        }
        if(h<=unite.dexterite && b<=unite.dexterite && d<=unite.dexterite && g<=unite.dexterite ) {
            //printf("REC | h = %d, b = %d, g = %d, d = %d\n", h, b, g, d);
            possibleCases(unite, jeu, nb, cases, h+1, b, d, g); // GAUCHE 
            possibleCases(unite, jeu, nb, cases, h, b+1, d, g); // HAUT
            possibleCases(unite, jeu, nb, cases, h, b, d+1, g); // DROITE
            possibleCases(unite, jeu, nb, cases, h, b, d, g+1); // DROITE
        }
    }

    //cout<<"AI FINI DE REMPLIR LE TABLEAU DES CASES POSSIBLES"<<endl;
}


bool isPossibleCase(int x_new, int y_new, Unite* unite, Jeu* jeu) {

    //cout<<" x_new = "<<x_new<<" y_new = "<<y_new<<" x_actf = "<<unite->coordonnees[1]<<" y_actif = "<<unite->coordonnees[0]<<endl;
    //cout<<"VÉRIFIE QUE LA CASE ("<<x_new<<", "<<y_new<<") EST A PORTÉÉ DE DÉPL POUR : "<<endl;
    //lireUnite(*unite);

    bool isEmpty = true;
    bool inMap = true;
    bool withinDext = true;
    bool notMount = true;

    // VERIFIE QU'IL N'Y PAS UNE UNITE SUR LA CASE
    for(int i=0 ; i<jeu->allUnits->taille ; i++) {
        if(x_new == jeu->allUnits->unites[i].coordonnees[1] && y_new == jeu->allUnits->unites[i].coordonnees[0]) {
            isEmpty = false;
            break;
        }
    }
    // VERIFIE QUE LA CASE EST DANS LA CARTE
    if(x_new<0 || x_new>=MAP_SIZE || y_new<0 || y_new>=MAP_SIZE) {
        inMap = false;
    }
    // VERIFIE QUE LA CASE EST ATTEIGNABLE AVEC LA DEXT DE L'UNITE
    
    //printf("x = %d, x_new = %d, y= %d, y_new = %d\n", unite->coordonnees[1], x_new, unite->coordonnees[0], y_new);
    if(abs(unite->coordonnees[1]-x_new)+abs(unite->coordonnees[0]-y_new) > unite->dexterite) {
        withinDext = false;
    }
    // VERIFIE QUE LA CASE N'EST PAS UNE MONTAGNE
    if(inMap && jeu->carte[y_new][x_new] == 2) {
        notMount = false;
    }

    //printf("isEmpty : %d, inMap : %d, withinDext : %d, notMount: %d\n", isEmpty, inMap, withinDext, notMount);

    // SI TOUTES LES CONDITIONS SONT REMPLIES, ON PEUT DEPLACER L'UNITE SUR LA CASE
    if(isEmpty && inMap && withinDext && notMount) {
        //cout<<"POSSIBLE\n";
        return true;
    }
    else {
        //cout<<"PAS POSSIBLE\n";
        return false;
    }
}


bool movePossible(int x_clic, int y_clic, Unite* unite, Jeu* jeu) {
    cout<<"REGARDE SI L'UNITÉ PEUT SE DÉPLACER SUR LA CASE CLIQUÉE"<<endl;
    int** cases = NULL;
    int nb = 0;
    possibleCases(*unite, jeu, &nb, cases, 0, 0, 0, 0);
    if(cases == NULL) {
        cout<<"ERREUR : pointeur nul"<<endl;
       return false;
    }
    cout<<"TAB DES CASES REMPLI"<<endl;
    for(int i=0 ; i<nb ; i++) {
        if(x_clic == cases[i][1] && y_clic == cases[i][0]) {
            return true;
        }
    }
    return false;
}




// SI LA CASE CLIQUÉE DE COORD (x,y) DANS LA CARTE A L'UNITE SELECTIONNÉE DESSUS, MET EN SURBRILLANCE LES CASES OU CELLE-CI PEUT SE DEPLACER
void highlightAtkCases(Jeu* jeu, GLuint texture, int indexSelectedUnit, Joueur* activePlayer) {
    
    if(indexSelectedUnit<0 ) { return; }
    Unite* unite = &(activePlayer->armee->unites[indexSelectedUnit]);

    if(!unite || !unite->current || !unite->utilisee[0]) { return; }
    int nb = 0;
    int** cases = casesWithinAtkRange(*unite, &nb, jeu, *activePlayer);
    if(cases==NULL) {
        cout<<"ERREUR : pointeur nul";
        return;
    }

    for(int j=0 ; j<nb ; j++) {
        highlight(cases[j][1], cases[j][0], texture);
    }
}

int** casesWithinAtkRange(Unite unite, int* nb, Jeu *jeu, Joueur activePlayer) {
    
    int** cases_atk = (int**) malloc(sizeof(int*));
    *nb = 0;

    for(int x=-unite.portee ; x<=unite.portee ; x++) {
        for(int y=-unite.portee ; y<=unite.portee ; y++) {
            // SI À PORTÉE ET N'EST PAS UNE CASE MONTAGNE
            // VERIFIE QUE LA CASE EST DANS LA CARTE

            bool withinRange = true, notMount = true, inMap = true;

            // CASE QU'ON VÉRIFIE
            int x_new = unite.coordonnees[1] + x;
            int y_new = unite.coordonnees[0] + y;


            /*printf("(x_unit, y_unit) = (%d, %d)\n", unite.coordonnees[1], unite.coordonnees[0]);
            printf("(x, y) = (%d, %d)\n", x, y);
            printf("(x_case, y_case) = (%d, %d)\n", x_new, y_new);*/

            if(x_new<0 || x_new>=MAP_SIZE || y_new<0 || y_new>=MAP_SIZE) {
                inMap = false;
            }

            if(abs(unite.coordonnees[1]-x_new)+abs(unite.coordonnees[0]-y_new) > unite.portee) {
                withinRange = false;
            }
  
            if(inMap && jeu->carte[x_new][y_new] == 2) {
                notMount = false;
            }
            if(inMap) {
                printf("Type : %d ", jeu->carte[x_new][x_new]);
            }

            printf("(%d, %d) | inMap : %d, withinRange : %d, notMount : %d\n", x_new, y_new, inMap, withinRange, notMount);

            if(inMap && withinRange && notMount) {
                
                cases_atk = (int**) realloc(cases_atk, sizeof(int*)*(50));
                cases_atk[*nb] = (int*) malloc(sizeof(int)*2);
                cases_atk[*nb][1] = x_new;
                cases_atk[*nb][0] = y_new;
                (*nb)++;
            }
        }
    }

    return cases_atk;
}

bool attackAfterMovePossible(Unite yourUnit, Jeu* jeu, int id_actif) {

    cout<<"REGARDE SI ENNEMI À PORTÉE D'ATK POUR NOTRE UNITE SELECTIONNÉE: "<<endl;
    lireUnite(yourUnit);

    int* nb = (int*) malloc(sizeof(int));
    int** cases = casesWithinAtkRange(yourUnit, nb, jeu, *jeu->joueurs[id_actif-1]);

    
    for(int i=0 ; i<*nb ; i++) {
        for(int j=0 ; j<jeu->joueurs[id_actif%2]->armee->taille ; j++) {
            // S'IL Y UN ENNEMI DANS LES CASES À PORTÉE D'ATTAQUE
            if(jeu->joueurs[id_actif%2]->armee->unites[j].coordonnees[1] == cases[i][1] && jeu->joueurs[id_actif%2]->armee->unites[j].coordonnees[0] == cases[i][0]) {
                cout<<"C'EST LE CAS"<<endl;
                return true;
            }
        }
    }
    return false;
}




void unitAction(Jeu* jeu, int id_actif, int x_clic, int y_clic, int* indexSelectedUnit) {

    cout<<"CLIC EN (x, y) = ("<<x_clic<<", "<<y_clic<<")"<<endl;

    Unite* enemyUnit = NULL;
    cout<<"ENNEMI POUR L'INSTANT NULL"<<endl;

    Unite* unite = &(jeu->joueurs[id_actif-1]->armee->unites[*indexSelectedUnit]);
    cout<<"L'UNITEÉ SÉLECTIONNÉE : "<<endl;
    lireUnite(*unite);
    // SI L'UNITE SELECTIONNÉ PEUT SE DÉPLACER SUR LA CASE CLIQUÉE
    cout<<"DÉPLACEMENT ?"<<endl;
    // SI L'UNITE SELECTIONNÉ PEUT ATTAQUER L'UNITÉ SUR LA CASE CLIQUÉE
    cout<<"ATTAQUE ?"<<endl;


    if(x_clic == unite->coordonnees[1] && y_clic == unite->coordonnees[0]) {
        cout<<"ON A CLIQUÉ SUR LA MÊME UNITÉ"<<endl;
        return;
    }


    else if(movePossible(x_clic, y_clic, unite, jeu) && !unite->utilisee[0]) {
        cout<<"L'UNITÉ SÉLECTIONNÉE PEUT SE DÉPLACER SUR LA CASE CLIQUÉE\n";
        deplacement(unite, x_clic, y_clic);
        unite->utilisee[0] = true;
        cout<<"ELLE S'EST DÉPLACÉE"<<endl;

        if(!attackAfterMovePossible(*unite, jeu, id_actif)) {
            cout<<"PAS D'UNITÉ ENEMIE À PORTÉE, L'UNITÉ SÉLECTIONNÉE N'A PLUS D'ACTIONS DONC DESELECTIONNE\n";
            unite->utilisee[1] = true;
            unite->current = false;
            //*indexSelectedUnit = -1;
        }
    }

    else if(isEnemyWithinRange(x_clic, y_clic, id_actif%2+1, unite, enemyUnit, jeu)) {
        cout<<"L'UNITE SÉLECTIONNÉE PEUT ATTAQUER LA CASE CLIQUÉE"<<endl;
        attaque(unite, enemyUnit, jeu, id_actif);
        cout<<"ELLE A ATTAQUÉ, PLUS D'ACTIONS POSSIBLES DONC DESELECTIONNE"<<endl;
        unite->utilisee[0] = true;
        unite->utilisee[1] = true;
        unite->current = false;
        //*indexSelectedUnit = -1;
        // REINIT LA SURBRILLANCE
    }
    else {
        cout<<"ON N'A NI RECLIQUÉ SUR LA MÊME UNITÉ NI DÉPLACER NI ATTAQUER DONC ON A CLIQUÉ AUTREPART, DÉSELECTIONNE"<<endl;
        unite->current = false;
        if(!unite->utilisee[1]) {
            unite->utilisee[1] = true;
        }
        //*indexSelectedUnit = -1;
    }

  
    // DEPLACER PUIS ATTAQUER
    // SI CLIQUE AUTRE PART, FINI
    
    cout<<"FIN D'UNIT ACTION"<<endl;
}





int isYourUnusedUnit(int x_clic, int y_clic, int id_actif, Jeu* jeu) {
    //cout<<"xclic "<<x_clic<<" yclic "<<y_clic;

    cout<<"REINITIALISE LES CURRENT DU JOUEUR ACTIF"<<id_actif<<endl; 
    reinitCurrent(jeu->joueurs[id_actif-1]);
    cout<<"REGARDE SI LA CASE CLIQUÉE PEUT ÊTRE SÉLECTIONNÉE (x, y) = ("<<x_clic<<", "<<y_clic<<")"<<endl;
    for(int i=0 ; i<jeu->joueurs[id_actif-1]->armee->taille ; i++) {

        //printf("xc=%d, yc=%d, x=%d, y=%d\n", x_clic, y_clic, jeu->joueurs[id_actif-1]->armee->unites[i].coordonnees[1], jeu->joueurs[id_actif-1]->armee->unites[i].coordonnees[0]);

        // REGARDE SI LES COORD DE LA CASE SELECTIONNEE CORRESPOND AUX COORD D'UNE UNITE DU JOUEUR ACTIF
        if(jeu->joueurs[id_actif-1]->armee->unites[i].coordonnees[0] == y_clic && jeu->joueurs[id_actif-1]->armee->unites[i].coordonnees[1] == x_clic) {
            // SI ELLE N'A PAS DÉJÀ ÉTÉ UTILISÉE
            if(!jeu->joueurs[id_actif-1]->armee->unites[i].utilisee[0] || !jeu->joueurs[id_actif-1]->armee->unites[i].utilisee[1]) {
                jeu->joueurs[id_actif-1]->armee->unites[i].current = true;
                jeu->allUnits = initallUnits(jeu);
                cout<<"UNE UNITÉ A ÉTÉ SÉLECTIONNÉE ET SON INDEX EST : "<<i<<endl;
                //cout<<"VERIF : "<<jeu->joueurs[id_actif-1]->armee->unites[i].current<<endl;
                return i;
            }
        }
    }
    cout<<"AUCUNE UNITÉ N'A ÉTÉ SÉLECTIONNÉ"<<endl;
    return -1;
}

// REGARDE S'IL Y A UN ENNEMI À PORTÉÉ D'ATTAQUE SUR LA CASE DE COORD x, y DANS LA CARTE
bool isEnemyWithinRange(int x, int y, int id_enemy, Unite* yourUnit, Unite*& enemyUnit, Jeu* jeu) {

    cout<<"VERIFIE SI LA CASE ("<<x<<", "<<y<<") A UNE UNITÉ ENEMI A PORTEE D'ATQ"<<endl;
    for(int i=0 ; i<jeu->joueurs[id_enemy-1]->armee->taille ; i++) {
        if(jeu->joueurs[id_enemy-1]->armee->unites[i].coordonnees[0] == y && jeu->joueurs[id_enemy-1]->armee->unites[i].coordonnees[1] == x) {
            if(abs(yourUnit->coordonnees[1]-x)+abs(yourUnit->coordonnees[0]-y) <= yourUnit->portee) {

                cout<<"C'EST LE CAS, IL S'AGIT DE : "<<endl;
                    enemyUnit = &(jeu->joueurs[id_enemy-1]->armee->unites[i]);
                    lireUnite(*enemyUnit);
                    return true;
            }
            else {
                break;
            }
        }
    }
    cout<<"CE N'EST PAS LE CAS"<<endl;
    return false;

}

void deplacement(Unite* unite_active, int x_clic, int y_clic) {
    unite_active->coordonnees[0] = y_clic;
    unite_active->coordonnees[1] = x_clic;
}


void attaque(Unite* yourUnit, Unite* enemyUnit, Jeu* jeu, int id_actif) {

    // PV_RESTANT = PV_INIT - PV_AUTRE * FORCE_AUTRE

    enemyUnit->pv = enemyUnit->pv - yourUnit->pv * yourUnit->force;
    if(abs(yourUnit->coordonnees[1]-enemyUnit->coordonnees[1]) + abs(yourUnit->coordonnees[0]-enemyUnit->coordonnees[0]) <= enemyUnit->portee) {
        yourUnit->pv = yourUnit->pv - enemyUnit->pv * enemyUnit->force;
    }
    
    cout << "PV(Unite ennemie) = "<<enemyUnit->pv << endl;
    cout << "PV(Votre unite) = " <<yourUnit->pv << endl;

    //SUPPRIME LES UNITES QUI ONT PERDU TOUS LEUR PV
    if(enemyUnit->pv<=0) {
        supprimerUnite(jeu->joueurs[id_actif%2]->armee, enemyUnit);
        cout << "Unite ennemie éliminée." << endl;
    }
    if(yourUnit->pv<=0) {
        supprimerUnite(jeu->joueurs[id_actif-1]->armee, yourUnit);
        cout << "Votre unité éliminée." << endl;
    }
  
}

void greyUsedUnits(Joueur activePlayer, GLuint texture) {

    for(int i=0 ; i<activePlayer.armee->taille ; i++) {
        if(activePlayer.armee->unites[i].utilisee[0] && activePlayer.armee->unites[i].utilisee[1]){

        int x = activePlayer.armee->unites[i].coordonnees[1];
        int y = activePlayer.armee->unites[i].coordonnees[0];

        //printf("USED UNIT (x, y) = (%d, %d)\n", x, y);
        highlight(x, y, texture);
        }
    }
}





Armee* initallUnits(Jeu* jeu) {
    
    Armee* allUnits = (Armee*) malloc(sizeof(Armee));
    // A POUR TAILLE LA SOMME DES TAILLES DES ARMEES 1 ET 2
    allUnits->taille = jeu->joueurs[0]->armee->taille + jeu->joueurs[1]->armee->taille;
    allUnits->unites = (Unite*) malloc(sizeof(Unite)*allUnits->taille);

    // MET TOUTES LES UNITES DE JOUEUR 1
    for(int i=0 ; i<jeu->joueurs[0]->armee->taille ; i++) { allUnits->unites[i] = jeu->joueurs[0]->armee->unites[i];  }
    // MET TOUTES LES UNITES DE JOUEUR 2
    for(int i=0 ; i<jeu->joueurs[1]->armee->taille ; i++) { allUnits->unites[i+jeu->joueurs[0]->armee->taille] = jeu->joueurs[1]->armee->unites[i]; }
    
    cout<<"INIT ALLUNTIS SUCCES"<<endl;
    return allUnits;
}

void supprimerUnite(Armee *armee, Unite* unite) {
    
    int index = -1;
    while(unite->coordonnees[0] != armee->unites[index].coordonnees[0] || unite->coordonnees[1] != armee->unites[index].coordonnees[1]) {
        index++;
    }
    if(index<0) {
        cout<<"ERREUR";
    }
    else if(index != (armee->taille)-1) {
        for(int i=index ; i<(armee->taille)-1 ; i++) {
            armee->unites[i] = armee->unites[i+1];
        }
    }
    
    armee->taille--;
}


void drawEndPanel(int winner, Jeu* jeu) {

    float r = 1, g = 1, b = 1;
    char text[50] = "BRAVO ";
    strcat(text, jeu->joueurs[winner-1]->nom);
    strcat(text, " ! TU AS GAGNE LA PARTIE");

    char redo[50] = "RECOMMENCER <R>";
    char quit[50] = "QUITTER <Q>";

    // JOUEUR 1 EN BLEU
    if(winner == 1) {
        r = 64/255.;
        g = 174/255.;
        b = 247/255.;
    }
    // JOUEUR 2 EN ROUGE
    else {
        r = 245/255.;
        g = 76/255.;
        b = 47/255.;
    }
    cout<<"GAGNANT : "<<winner<<endl;
    cout<<"STRLEN : "<<strlen(redo)<<endl;
    bitmapOutput(-38, 20, r, g, b, text, GLUT_BITMAP_HELVETICA_18);
    bitmapOutput(-18, 0, 1, 1, 1, redo, GLUT_BITMAP_9_BY_15);
    bitmapOutput(-15, -10, 1, 1, 1, quit, GLUT_BITMAP_9_BY_15);
}

void infoPanel() {
    glPushMatrix();
        glTranslatef(-WINDOW_WIDTH*R/2., 0., 0.);
        bitmapOutput(92, -WINDOW_HEIGHT*R/2. + 10, 1, 1, 1, "RETOUR <I>", GLUT_BITMAP_9_BY_15);
        glTranslatef(-5, 30, 0);
        bitmapOutput(70, 40, 1, 1, 1, "INFORMATIONS SUR LES CASES", GLUT_BITMAP_HELVETICA_18);
        bitmapOutput(45, 20, 1, 1, 1, "LES CASES FORET REDUISENT LA MOBILITE DES CAVALIERS", GLUT_BITMAP_9_BY_15);
        bitmapOutput(45, 0, 1, 1, 1, "LES CASES MONTAGNE SONT DES OBSTACLES INFRANCHISSABLES", GLUT_BITMAP_9_BY_15);
        bitmapOutput(45, -20, 1, 1, 1, "LES CASES FORTERESSE DONNENT UN BONUS DE 0.1 EN FORCE", GLUT_BITMAP_9_BY_15);
        bitmapOutput(70, -60, 1, 1, 1, "INFORMATIONS SUR LES UNITES", GLUT_BITMAP_HELVETICA_18);
        bitmapOutput(35, -80, 1, 1, 1, "SURVOLER UNE UNITE POUR VOIR CES INFORMATIONS S'AFFICHER EN BAS", GLUT_BITMAP_9_BY_15);
        
    glPopMatrix();
    
}

// SI ATTAQUE POSSIBLE, RETIENT QU'ON PEUT PLUS LE DÉPLACER | OK

// GRISE LES CASES UTILISEES | OK
// CHANGE LE NOM DU JOUEUR ACTIF | OK
// SI ARCHER ATTAQIE N'EST PAS ATTAQUE SI A 2 CASES | OK
// ATTENTION UNITES PEUVENT ETRE INITIALISEES SUR UNE CASE MONTAGNE
// AFFICHER INFO QUAND SURVOLE 
// ENTRER LES NOMS DES JOUEURS, SINON PAR DÉFAUT JOUEUR 1 ET JOUEUR 2
// INDIQUER LES UNITES UTILISEES
// PASSER LE TOUR

