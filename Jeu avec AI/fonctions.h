#include "main.h"
//Nouvelles fonctions

bool isDestination(Coordonnees* current, Coordonnees* dest);
void calculCost(Coordonnees* first, Coordonnees *carte, Coordonnees* start, Coordonnees* dest, int taille_carte);
void calculHCostDiagonal(Coordonnees* start, Coordonnees *carte, int taille_carte);
void calculGCostDiagonal(Coordonnees* dest, Coordonnees *carte, int taille_carte);
int distanceCase(Coordonnees* start, Coordonnees* cellule);
void calculFCost(Coordonnees* autre);
int isDiagonal(Coordonnees* cellule, Coordonnees* start, int taille_carte);
bool isObstacle(Coordonnees* cellule, Armee* armeeInverse);
void tri(Coordonnees* tab[4]);

void artificialIntelligence(Joueur* joueur, Armee* armee1, Armee* armee2, Jeu* jeu);
int AStar(Coordonnees* start, Coordonnees* dest, Armee* armeeInverse, Armee* artificial, int taille_carte, int dexterite, Coordonnees* tabDep[]);

Coordonnees* createCase(int x, int y);
Coordonnees* lookSiblings(Coordonnees* first, Coordonnees* start, Coordonnees* dest, Coordonnees* to_visit[], Coordonnees* visited[], int taille_carte);
Coordonnees* tabDeplacement(int x, int y, Coordonnees* tab[]);
