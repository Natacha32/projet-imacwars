#define GL_SILENCE_DEPRECATION

#ifndef MAIN_H
#define MAIN_H

#include <iostream>
using namespace std;
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
//#include <OpenGL/gl.h>
//#include <OpenGL/glu.h>
//#include <GLUT/glut.h>


#define MAX_WIDTH

// CONSTANTES

// Nom de la fenetre
static const char WINDOW_TITLE[] = "IMACWARS";
// Dimensions de la fenetre
static const int WINDOW_WIDTH = 800;
static const int WINDOW_HEIGHT = 1000;
// Nombre de bits par pixel de la fenetre
static const unsigned int BIT_PER_PIXEL = 32;
// Nombre minimal de millisecondes separant le rendu de deux images
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;
// Pour resize
static float aspectRatio = WINDOW_WIDTH/(float)WINDOW_HEIGHT;
// Espace fenetre virtuelle
static const float GL_VIEW_SIZE = 200.;
// Infos carte
static int MAP_SIZE = 10;
static const int TYPE_LAND = 4;
static const int TYPE_UNITES = 4;
static float TILE_SIZE = GL_VIEW_SIZE/MAP_SIZE;
static int NB_UNIT = 6;
static float R = GL_VIEW_SIZE/WINDOW_WIDTH;

// STRUCTURES

typedef struct Unite {
    int id;
    int pv;
    float force;
    int dexterite;
    int portee;
    int coordonnees[2];
    bool utilisee[2]; //
    bool current;
    int type;
} Unite;

typedef struct Armee {
    Unite *unites;
    int taille;
    int joueur;
} Armee;

typedef struct Joueur {
    int id;
    int fond_guerre;
    Armee *armee;
    char nom[MAX_WIDTH];
} Joueur;

typedef struct Jeu {
    int **carte;
    Joueur* joueurs[2];
    Armee* allUnits;
} Jeu;

#endif