#include "tools.h"

Unite *creerFantassin(int joueur) {
    Unite *fantassin=(Unite*)malloc(sizeof(Unite));
    fantassin->id = joueur;
    fantassin->pv = 100;
    fantassin->force = 0.6;
    fantassin->dexterite = 1;
    fantassin->portee= 1;
    fantassin->utilisee[0] = false;
    fantassin->utilisee[1] = false;
    fantassin->current = false;
    fantassin->type = 1;
    return fantassin;
}

Unite *creerCavalier(int joueur) {
    Unite *cavalier=(Unite*)malloc(sizeof(Unite));
    cavalier->id = joueur;
    cavalier->pv = 100;
    cavalier->force = 0.4;
    cavalier->dexterite = 2;
    cavalier->portee = 1;
    cavalier->utilisee[0] = false;
    cavalier->utilisee[1] = false;
    cavalier->current = false;
    cavalier->type = 2;
    return cavalier;
}

Unite *creerArcher(int joueur) {
    Unite *archer=(Unite*)malloc(sizeof(Unite));
    archer->id = joueur;
    archer->pv = 100;
    archer->force = 0.4;
    archer->dexterite = 1;
    archer->portee = 2;
    archer->utilisee[0] = false;
    archer->utilisee[1] = false;
    archer->current = false;
    archer->type = 3; 
    return archer;
}

void coordIni(Jeu* jeu) {
    int x=0, y=0;

    for(int i=0;i<NB_UNIT;i++) {
        
        jeu->joueurs[0]->armee->unites[i].coordonnees[0]=x;
        jeu->joueurs[0]->armee->unites[i].coordonnees[1]=y;
        jeu->joueurs[1]->armee->unites[i].coordonnees[0]=x;
        jeu->joueurs[1]->armee->unites[i].coordonnees[1]=MAP_SIZE-1-y;

        x++;
        if(x==MAP_SIZE) {
            x=0;
            y++;
        }
        
    }
}

void ajouterUnites(Armee *armee) {
    armee->unites=(Unite*)malloc(armee->taille*sizeof(Unite));

    int nb_c = (armee->taille)/4;
    int nb_a = (armee->taille)/4;
    int nb_s = armee->taille - nb_c - nb_a;


    for(int i=0;i<nb_s;i++) {

        armee->unites[i]=*creerFantassin(armee->joueur);
    }
    for(int i=nb_s;i<nb_s+nb_c;i++) {

        armee->unites[i]=*creerCavalier(armee->joueur);
    }
    for(int i=nb_s+nb_c;i<armee->taille;i++) {

        armee->unites[i]=*creerArcher(armee->joueur);
    }
}


void lireUnite(Unite unite) {
    cout<<"type : ";
    if(unite.type == 83) {
        cout<<"soldat | ";
    }
    if(unite.type == 67) {
        cout<<"cavalier | ";
    }
    cout<<"coordonnees : "<<"("<<unite.coordonnees[1]<<", "<<unite.coordonnees[0]<<")"<<" | ";
    cout<<"pv : "<<unite.pv<<" | ";
    cout<<"force : "<<unite.force<<" | ";
    cout<<"dextérité : "<<unite.dexterite<<" | ";
    cout<<"utilisé[0] : ";
    if(unite.utilisee[0]) {
        cout<<"oui ";
    }else {
        cout<<"non ";
    }
    cout<<"utilisé[1] : ";
    if(unite.utilisee[1]) {
        cout<<"oui ";
    }
    else {
        cout<<"non ";
    }
    cout<<" | current : ";
    if(unite.current==true) {
        cout<<"oui\n";
    }
    else {
        cout<<"non\n";
    }
}

void lireArmee(Armee *armee) {
    for(int i=0;i<armee->taille;i++) {
        lireUnite(armee->unites[i]);
    }
}

Joueur* initPlayer(int id, int fond) {
    Joueur* joueur = (Joueur*) malloc(sizeof(Joueur));
    joueur->id = id;
    joueur->fond_guerre = fond;

    Armee* armee = (Armee*) malloc(sizeof(Armee));
    armee->taille = NB_UNIT;
    armee->joueur = id;
    joueur->armee = armee;

    ajouterUnites(joueur->armee);
    return joueur;
}


bool usedAllUnits(Armee* armee) {
    for(int i=0 ; i<armee->taille ; i++) {
        if(!armee->unites[i].utilisee[0] || !armee->unites[i].utilisee[1]) {
            return false;
        }
    }
    return true;
}

void reinitUtilisee(Joueur* joueur) {
    for(int i=0; i<joueur->armee->taille;i++) {
        joueur->armee->unites[i].utilisee[0]=false;
        joueur->armee->unites[i].utilisee[1]=false;
    }
}


void reinitCurrent(Joueur* joueur) {
    for(int i=0; i<joueur->armee->taille;i++) {
        joueur->armee->unites[i].current=false;
    }
}

int endGame(int taille1, int taille2) {
    if(taille1<=0 && taille2<=0){
        return 3; // terminé: match nul
    }
    else if(taille1<=0) {
        return 2; // terminé : gagnant joueur 2
    }
    else if(taille2<=0) {
        return 1; // terminé : gagnant joueur 1
    }
    else return 0; //pas terminé
}




// AFFICHE UNE TAB 2D DE DIMENSIONS n*m
void displayArray2D(int** tab2D, int n, int m) {
    for(int i=0 ; i<n; i++) {
        for(int j=0 ; j<m; j++) {
            printf("%d ", tab2D[i][j]);
        }
        printf("\n");
    }
}

// GENERE UN NB ALEATOIRE ENTRE 0 ET n-1 AVEC CERTAINES PROB STOCKEES DANS p[]
int randomNumber(int n, float p[]) { 

    int r = (rand() % 100);
    int s = p[0], r_nb = 0;
    for(int i=0 ; i<n ; i++) {
        s += p[i+1];
        if(r>=s-p[i+1] && r<s) {
            r_nb = i;
            break;
        }
    }

    return r_nb;
}

// GENERE LE TABLEAU 2D DE LA CARTE DE DIM n*n
// 0->plaine, 1->foret2, 2->montagne, 3->forteresse 
int** createMap() {

    // ALLOCATION DYNAMIQUE D'UN TABLEAU 2D
    int** carte = (int**) malloc(sizeof(int*)*MAP_SIZE);
    for(int i=0 ; i<MAP_SIZE; i++) {
        carte[i] = (int*) malloc(sizeof(int)*MAP_SIZE);
    } 

    // TAB DES PROBABILITES D'AVOIR TEL TYPE DE CASE
    // SOMME = 100
    float p[5] = {0, 82, 10, 4, 4}; 

    // REMPLIT ALÉATOIREMENT MAIS DE FAÇON PONDÉRÉE LA CARTE AVEC LES TYPES DE TERRAIN
    for(int i=0 ; i<MAP_SIZE; i++) {
        for(int j=0 ; j<MAP_SIZE; j++) {
            carte[i][j] = randomNumber(TYPE_LAND, p);
        }
    }

    return carte;
}

// APPLIQUE LES EFFETS DES CASES AUX UNITES QUI SE TROUVENT DESSUS 
void CaseEffectsOn(Jeu* jeu) {

    for(int p=0 ; p<2 ; p++) {
        for(int i=0 ; i<jeu->joueurs[p]->armee->taille ; i++) {
            int x = jeu->joueurs[p]->armee->unites[i].coordonnees[0];
            int y = jeu->joueurs[p]->armee->unites[i].coordonnees[1];
            // SI L'UNITE DE TYPE CAVALIER ET SE TROUVE SUR UNE CASE FORET
            if(jeu->carte[x][y] == 1 && jeu->joueurs[p]->armee->unites[i].type == 2) {
                jeu->joueurs[p]->armee->unites[i].dexterite = 1;
            }
            // SI L'UNITE SUR UNE CASE FORTERESSE
            if(jeu->carte[x][y] == 3) {
                cout<<"CASE FORTERESSE ATK+0.1. L'UNITÉ CONCERNÉE EST : "<<endl;
                jeu->joueurs[p]->armee->unites[i].force = jeu->joueurs[p]->armee->unites[i].force + 0.1;
                lireUnite(jeu->joueurs[p]->armee->unites[i]);
            }
        }
    }
}

void reinitStats(Jeu* jeu) {
    for(int p=0 ; p<2 ; p++) {
        for(int i=0 ; i<jeu->joueurs[p]->armee->taille ; i++) {
            if(jeu->joueurs[p]->armee->unites[i].type == 1) {
                jeu->joueurs[p]->armee->unites[i].force = 0.6;
            }
            else if(jeu->joueurs[p]->armee->unites[i].type == 2) {
                jeu->joueurs[p]->armee->unites[i].force = 0.4;
                jeu->joueurs[p]->armee->unites[i].dexterite = 2;
            }
            else {
                jeu->joueurs[p]->armee->unites[i].force = 0.4;
            }
        }
    }
}

Jeu* initGame() {

    Jeu* jeu = (Jeu*) malloc(sizeof(Jeu));
    jeu->carte = createMap();
    jeu->joueurs[0] = initPlayer(1, 100);
    jeu->joueurs[1] = initPlayer(2, 100);

    coordIni(jeu);

    jeu->allUnits = initallUnits(jeu);

    return jeu;
}





// PERMET DE METTRE LES IMAGES A LA BONNE TAILLE
void reshape(SDL_Surface** surface, unsigned int width, unsigned int height)
{
    SDL_Surface* surface_temp = SDL_SetVideoMode(
        width, height, BIT_PER_PIXEL,
        SDL_OPENGL | SDL_GL_DOUBLEBUFFER | SDL_RESIZABLE);
    if(NULL == surface_temp)
    {
        fprintf(
            stderr,
            "Erreur lors du redimensionnement de la fenetre.\n");
        exit(EXIT_FAILURE);
    }
    *surface = surface_temp;

    glViewport(0, 0, (*surface)->w, (*surface)->h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if( aspectRatio > 1)
    {
        gluOrtho2D(
        -GL_VIEW_SIZE / 2. * aspectRatio, GL_VIEW_SIZE / 2. * aspectRatio,
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.);
    }
    else
    {
        gluOrtho2D(
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.,
        -GL_VIEW_SIZE / 2. / aspectRatio, GL_VIEW_SIZE / 2. / aspectRatio);
    }
}

// CHARGE L'IMAGE
SDL_Surface* image(const char* img_path) {

    SDL_Surface* img = IMG_Load(img_path);
    if(!img) {
      printf("Echec du chargement de l'img %s.\n", img_path);
      exit(EXIT_FAILURE);
    }

    return img;
}

// INITIALISE LA TEXTURE
int initTexture(SDL_Surface* image, GLuint* texture) {

    // ATTACHE AU POINT DE BIND
    glBindTexture(GL_TEXTURE_2D, *texture);

    // MODIFIE LES PARAM
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    GLenum format;
    switch(image->format->BytesPerPixel) {
        case 1:
            format = GL_RED;
            break;
        case 3:
            format = GL_RGB;
            break;
        case 4:
            format = GL_RGBA;
            break;
        default:
            printf("Format des pixels de l'image non supporte.\n");
            return EXIT_FAILURE;
    }

    // ENVOIE LES DONNEES DE L'IMG VERS LE GPU
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, format, GL_UNSIGNED_BYTE, image->pixels);

    // DETACHE UNE FOIS IMG CHARGEE
    glBindTexture(GL_TEXTURE_2D, 0);

    return EXIT_SUCCESS;
}

// DESSINE UNE CASE DE TAILLE TILE_SIZE
void drawCase() {
    glBegin(GL_QUADS);

	glTexCoord2f(0, 1);
    glVertex2f(0., -TILE_SIZE);

    glTexCoord2f(1, 1);
    glVertex2f(TILE_SIZE, -TILE_SIZE);

    glTexCoord2f(1, 0);
    glVertex2f(TILE_SIZE, 0.);

    glTexCoord2f(0, 0);
    glVertex2f(0., 0.);

	glEnd();
}

// AFFICHE TOUTES LES CASES DU TYPE DE TERRAIN PASSE EN ARGUMENT
void drawCasesOfType(int type, GLuint texture, int** carte) {

    glBindTexture(GL_TEXTURE_2D, texture);

    for(int i=0 ; i<MAP_SIZE; i++) {
        for(int j=0 ; j<MAP_SIZE; j++) {
            if(carte[i][j] == type) {
                glPushMatrix();
                    glTranslatef(-GL_VIEW_SIZE/2+j*TILE_SIZE, GL_VIEW_SIZE/2./aspectRatio-i*TILE_SIZE, 0.);
                    drawCase();
                glPopMatrix();
            }
        }
    }

    glBindTexture(GL_TEXTURE_2D, 0);
}

// AFFICHES TOUTES LES UNITES DU TYPE PASSE EN ARGUMENT ***
void drawUnitType(Armee *armee, GLuint texture, int type) {

    glBindTexture(GL_TEXTURE_2D, texture);

    int x=0, y=0;
    for(int i=0 ; i<armee->taille ; i++) {
        if(armee->unites[i].type == type) {
            y = armee->unites[i].coordonnees[0]*TILE_SIZE;
            x = armee->unites[i].coordonnees[1]*TILE_SIZE;
            glPushMatrix();
                glTranslatef(-GL_VIEW_SIZE/2+x, GL_VIEW_SIZE/2./aspectRatio-y, 0.);
                drawCase();
            glPopMatrix();
        }
    }

    glBindTexture(GL_TEXTURE_2D, 0);
}

// MET EN SURBRILLANCE LES CASES (x, y) SURVOLE
void highlight(int x, int y, GLuint texture) {

    //printf("(x,y) = (%d, %d)\n", x, y);
    // Si X ET Y DANS LA CARTE
    if(x>=0 && y>=0 && x<MAP_SIZE && y<MAP_SIZE) {

        // ATTACHE LE TEXTURING AU POINT DE BIND
        glBindTexture(GL_TEXTURE_2D, texture);

            glPushMatrix();
                glTranslatef(-GL_VIEW_SIZE/2.+x*TILE_SIZE, GL_VIEW_SIZE/2./aspectRatio-y*TILE_SIZE, 0.);
                drawCase();
            glPopMatrix();

        // DETACHE LE TEXTURING
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}




// ECRIT UN TEXTE
void bitmapOutput(float x, float y, float r, float g, float b, const char *string, void *font) {

    // COULEUR DU TEXTE
    glColor3d(r, g, b);
    // POSITIONNE LE jeu->joueurs[0] CARACTÈRE DE LA CHAINE EN x, y
	glRasterPos2f(x, y);

    // AFFICHE CHAQUE CARACTÈRE UN PAR UN
	for (int i = 0; i < strlen(string) ; i++) {
        // AFFICHE LE CARACTERE À LA POS i DANS LA CHAINE
        glutBitmapCharacter(font, string[i]);
    }

    glFlush(); 
}

void menuInGame(int x_clic, int y_clic) {


    // ECRIT ET POSITIONNE LE TEXTE

    float h = 15;
    float a = (-((WINDOW_HEIGHT-WINDOW_WIDTH)/4.)+h/2.)*R;
    
    // PLACE LE COIN INF GAUCHE DE LA PREMIERE LIGNE AVEC LE COIN SUP GAUCHE DE LA FENETRE
    drawClickText(5, -R*WINDOW_WIDTH+a, h*R, x_clic, y_clic, "RECOMMENCER <R>");
    drawClickText(5, -R*WINDOW_WIDTH+2*a, h*R, x_clic, y_clic, "PASSER SON TOUR <P>");
    drawClickText(5, -R*WINDOW_WIDTH+3*a, h*R, x_clic, y_clic, "INFO SUR LES CASES <I>");
    drawClickText(5, -R*WINDOW_WIDTH+4*a, h*R, x_clic, y_clic, "QUITTER <Q>");
}

void showSelectedUnitInfo(int x, int y, Armee* allUnits) {

    string carac;
    // PREPARE LE TEXTE DES CARACTERISTIQUES
    for(int i=0 ; i<allUnits->taille ; i++) {
        if(allUnits->unites[i].coordonnees[0] == y && allUnits->unites[i].coordonnees[1] == x) {
            carac = "TYPE "  + to_string(allUnits->unites[i].type) + " | PV " + to_string(allUnits->unites[i].pv)
            + " | FORCE " + to_string(allUnits->unites[i].force) + " | DEXT " + to_string(allUnits->unites[i].dexterite)
            + " | PORTEE " + to_string(allUnits->unites[i].portee);
            break;
        }
    }

    float h = 15;
    float a = (-((WINDOW_HEIGHT-WINDOW_WIDTH)/2.)-h/2.)*R;

    bitmapOutput(75, -R*WINDOW_WIDTH+a, 1, 1, 1, carac.c_str(), GLUT_BITMAP_9_BY_15);
}

void showActivePlayer(Joueur joueur) {

    char nom[58] = "JOUEUR ";
    strcat(nom, joueur.nom);

    float r = 1, g = 1, b = 1;

    // JOUEUR 1 EN BLEU
    if(joueur.id == 1) {
        r = 64/255.;
        g = 174/255.;
        b = 247/255.;
    }
    // JOUEUR 2 EN ROUGE
    else {
        r = 245/255.;
        g = 76/255.;
        b = 47/255.;
    }

    bitmapOutput(120, -R*WINDOW_WIDTH-10, r, g, b, nom, GLUT_BITMAP_HELVETICA_18);
}

// DESSINE UN TEXTE QUI CHANGE DE COULEUR QUAND ON CLIQUE DESSUS
void drawClickText(float x, float y, int hauteur, int x_clic, int y_clic, const char* string) {

    float largeur = strlen(string)*9*R;
    
    //cout<<"nb "<<strlen(string)<<" l "<<largeur<<endl;
    //printf("x_clic : %d, y_clic: %d, x_d : %f, x_f : %f, y_d : %f, y_f : %f\n", x_clic, y_clic, x, x+largeur, -y-hauteur, -y);
    
    if(x_clic>=x && x_clic<x+largeur && y_clic>-y-hauteur && y_clic<=-y) {
        bitmapOutput(x, y, 0, 1, 0, string, GLUT_BITMAP_9_BY_15);
    }
    else {
        bitmapOutput(x, y, 1, 1, 1, string, GLUT_BITMAP_9_BY_15);
    }

}